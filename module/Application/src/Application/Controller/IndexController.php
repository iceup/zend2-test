<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\TableGateway\TableGateway;

class IndexController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel();
    }

    public function dbtestAction() {
        //实例化适配器
        $dbAdapter = new \Zend\Db\Adapter\Adapter(array(
            'driver'              => "pdo_mysql",
            'hostname'            => "localhost",
            'port'                => 3306,
            'database'            => "test",
            'username'            => "root",
            'password'            => "",
            'charset'             => "UTF8",
            'driver_options.1002' => "SET NAMES utf8",
        ));

        //插入一条数据
        $tableGateway = new TableGateway('user', $dbAdapter);
        $result       = $tableGateway->insert(array('id' => 1, 'name' => 'tom'));
        var_dump($result);

        //查找刚插入的数据
        $result = $tableGateway->select(array('id=?' => 1))->toArray();
        var_dump($result);

        //更新数据
        $result = $tableGateway->update(array('name' => 'anny'), array('id' => 1));
        var_dump($result);

        $result = $tableGateway->select(array('id=?' => 1))->toArray();
        var_dump($result);

        //删除数据
        $result = $tableGateway->delete(array('id=?' => 1));
        var_dump($result);

        $result = $tableGateway->select(array('id=?' => 1))->toArray();
        var_dump($result);
    }

}
